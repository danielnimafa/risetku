package com.example.androidapplikegmail.network;

import com.example.androidapplikegmail.model.Message;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by danielnimafa on 2/28/17.
 */

public interface ApiInterface {

    @GET("inbox.json")
    Call<List<Message>> getInbox();

}
