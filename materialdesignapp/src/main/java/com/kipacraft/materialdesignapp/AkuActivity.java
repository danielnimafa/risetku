package com.kipacraft.materialdesignapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by danielnimafa on 2/7/17.
 *
 * this activity contains about implement custom layout menu
 */

public class AkuActivity extends AppCompatActivity {

    private EditText einput;
    private Button btSubmit;
    private TextView txBadge;
    private Toolbar toolbar;
    private ActionBar ab;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aku_layout);
        // bind view
        einput = (EditText) findViewById(R.id.input_badge);
        btSubmit = (Button) findViewById(R.id.bt_submit);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        // setup view
        setSupportActionBar(toolbar);
        ab = getSupportActionBar();
        // add view listener
        einput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    txBadge.setText(s);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem menuItem = menu.findItem(R.id.action_settings);
        RelativeLayout rootView = (RelativeLayout) menuItem.getActionView();
        txBadge = (TextView) rootView.findViewById(R.id.badge);
        return true;
    }
}
