package com.kipacraft.risetku.androidrealm.helper;

import io.realm.DynamicRealm;
import io.realm.RealmMigration;
import io.realm.RealmSchema;

/**
 * Created by danielnimafa on 3/7/17.
 */

public class RealmMigrate implements RealmMigration {

    public RealmMigrate() {
    }

    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        System.out.println("oldVersion: " + oldVersion);

        RealmSchema schema = realm.getSchema();

        if (oldVersion == 1) {
            schema.get("AkunData").addField("isHidden", boolean.class);
            oldVersion++;
        }
    }
}
