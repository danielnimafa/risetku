package com.kipacraft.risetku.androidrealm.model;

import io.realm.RealmObject;

/**
 * Created by danielnimafa on 3/7/17.
 */

public class AkunData extends RealmObject {
    String namaAkun, username, password;
    boolean isHidden;

    public boolean isHidden() {
        return isHidden;
    }

    public void setHidden(boolean hidden) {
        isHidden = hidden;
    }

    public String getNamaAkun() {
        return namaAkun;
    }

    public void setNamaAkun(String namaAkun) {
        this.namaAkun = namaAkun;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
