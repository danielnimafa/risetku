package com.kipacraft.risetku.androidrealm.helper;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by danielnimafa on 3/7/17.
 */

public class RealmBuilder {

    public static Realm getRealmBuilder() {
        RealmConfiguration config = new RealmConfiguration.Builder()
                .schemaVersion(Const.SCHEMA_VERSION)
                .migration(new RealmMigrate())
                .build();
        return Realm.getInstance(config);
    }

}
