package com.kipacraft.risetku.androidrealm;

import android.app.Application;

import io.realm.Realm;

/**
 * Created by danielnimafa on 3/7/17.
 */

public class AppMain extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
