package com.kipacraft.risetku.androidrealm;

import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.chad.library.adapter.base.listener.OnItemLongClickListener;
import com.kipacraft.risetku.androidrealm.adapters.DataAkunAdapter;
import com.kipacraft.risetku.androidrealm.helper.RealmBuilder;
import com.kipacraft.risetku.androidrealm.model.AkunData;

import java.util.LinkedList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {

    private Realm realm;
    private RealmResults<AkunData> akunDatas;
    private LinkedList<AkunData> akunDatasList = new LinkedList<>();
    private DataAkunAdapter adapter;
    private ActionBar ab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash);
        ButterKnife.bind(this);
        initObj();
        setupView();
        setupAkunData();
        addViewListener();
        populateData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dash_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_input:
                if (drawerLayout.isDrawerOpen(GravityCompat.END)) {
                    drawerLayout.closeDrawer(navView);
                } else {
                    drawerLayout.openDrawer(GravityCompat.END);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupView() {
        setSupportActionBar(toolbar);
        ab = getSupportActionBar();
        ab.setTitle("Akunku");

        LinearLayoutManager lm = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,
                lm.getOrientation());
        listContact.setLayoutManager(lm);
        listContact.setHasFixedSize(true);
        listContact.addItemDecoration(dividerItemDecoration);
    }

    private void populateData() {
        listContact.setAdapter(adapter);

        if (akunDatasList.size() > 0) {
            akunDatasList.clear();
        }

        // load data
        akunDatas = realm.where(AkunData.class).findAll();
        System.out.println("akunDatas: " + akunDatas.size());
        if (akunDatas.size() > 0) {
            for (AkunData data : akunDatas) {
                akunDatasList.add(data);
            }
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    private void initObj() {
        realm = RealmBuilder.getRealmBuilder();
        adapter = new DataAkunAdapter(R.layout.item_layout, akunDatasList);
        akunDatas = realm.where(AkunData.class).findAll();
        akunDatas.addChangeListener(element -> {
            int size = element.size();
            System.out.println("AkunDatas: " + size);
        });
    }

    private void addViewListener() {
        listContact.addOnItemTouchListener(new OnItemLongClickListener() {
            @Override
            public void onSimpleItemLongClick(BaseQuickAdapter adapter, View view, int position) {
                String namaAkun = akunDatasList.get(position).getNamaAkun();
                realm.executeTransactionAsync(realm12 -> {
                    RealmResults<AkunData> results = realm12.where(AkunData.class)
                            .equalTo("namaAkun", namaAkun).findAll();
                    results.deleteAllFromRealm();
                }, () -> {
                    adapter.notifyDataSetChanged();
                });
            }
        });

        listContact.addOnItemTouchListener(new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                boolean isHidden = akunDatasList.get(position).isHidden();
                String namaAkun = akunDatasList.get(position).getNamaAkun();

                realm.executeTransactionAsync(realm1 -> {
                    AkunData queryItemData = realm1.where(AkunData.class)
                            .equalTo("namaAkun", namaAkun).findFirst();
                    queryItemData.setHidden(!isHidden);
                }, () -> {
                    adapter.notifyDataSetChanged();
                    StringBuilder str = new StringBuilder();
                    String itemPass = akunDatasList.get(position).getPassword();
                    for (int i = 0; i < itemPass.length(); i++) {
                        str.append("*");
                    }
                    TextView txPass = (TextView) view.findViewById(R.id.pass);
                    txPass.setText((isHidden) ? str.toString() : itemPass);
                });
            }
        });

        submit.setOnClickListener(v -> {
            StringBuffer errorMsg = new StringBuffer();

            String akun = inputNama.getText().toString();
            String email = inputEmail.getText().toString();
            String pass = inputPass.getText().toString();

            if (akun.length() == 0) {
                errorMsg.append("- Akun harus diisi");
            }

            if (email.length() == 0) {
                errorMsg.append("- username harus diisi");
            }

            if (pass.length() == 0) {
                errorMsg.append("- Password harus diisi");
            }

            if (errorMsg.length() > 0) {
                Toast.makeText(this, errorMsg.toString(), Toast.LENGTH_SHORT).show();
            } else {
                // execute input data
                realm.executeTransaction(realmObj -> {
                    AkunData akunData = realmObj.createObject(AkunData.class);
                    akunData.setNamaAkun(akun);
                    akunData.setUsername(email);
                    akunData.setPassword(pass);
                    akunData.setHidden(true);
                    resetFields();
                    populateData();
                });
            }
        });
    }

    private void refreshData() {

    }

    private void resetFields() {
        inputEmail.setText("");
        inputNama.setText("");
        inputPass.setText("");
        inputNama.requestFocus();
    }

    private void setupAkunData() {

    }

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.list_contact)
    RecyclerView listContact;
    @BindView(R.id.activity_main)
    RelativeLayout activityMain;
    @BindView(R.id.input_nama)
    EditText inputNama;
    @BindView(R.id.input_email)
    EditText inputEmail;
    @BindView(R.id.input_pass)
    EditText inputPass;
    @BindView(R.id.bt_visible)
    AppCompatImageButton btVisible;
    @BindView(R.id.bt_unvisible)
    AppCompatImageButton btUnvisible;
    @BindView(R.id.lay_upass)
    RelativeLayout layUpass;
    @BindView(R.id.submit)
    Button submit;
    @BindView(R.id.nav_view)
    RelativeLayout navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
}
