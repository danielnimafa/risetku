package com.kipacraft.risetku.androidrealm.adapters;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.kipacraft.risetku.androidrealm.R;
import com.kipacraft.risetku.androidrealm.model.AkunData;

import java.util.List;

/**
 * Created by danielnimafa on 3/7/17.
 */

public class DataAkunAdapter extends BaseQuickAdapter<AkunData, BaseViewHolder> {

    public DataAkunAdapter(int layoutResId, List<AkunData> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, AkunData item) {
        if (item != null) {
            StringBuilder str = new StringBuilder();
            for (int i = 0; i < item.getPassword().length(); i++) {
                str.append("*");
            }
            helper.setText(R.id.nama_akun, item.getNamaAkun());
            helper.setText(R.id.uname, item.getUsername());

            boolean isHidden = item.isHidden();
            helper.setText(R.id.pass, (isHidden) ? str.toString() : item.getPassword());
        }
    }
}
