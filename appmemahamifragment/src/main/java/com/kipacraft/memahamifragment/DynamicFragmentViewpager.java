package com.kipacraft.memahamifragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;

public class DynamicFragmentViewpager extends AppCompatActivity {

    List<View> listOfViews = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic_fragment_viewpager);
        ButterKnife.bind(this);

        for (int i = 0; i < 10; i++) {
            LinearLayout ll = new LinearLayout(this);
            ll.setOrientation(LinearLayout.HORIZONTAL);
            ll.setId(i);

            getSupportFragmentManager().beginTransaction().add(ll.getId(),
                    TestFragment.newInstance("Ini Fragment ke-" + i))
                    .commit();

            layContent.addView(ll);
            listOfViews.add(layContent);
        }

    }

    @Override
    public void onBackPressed() {
        int count = listOfViews.size() - 1;
        if (count == 0) {
            finish();
        } else {
            int index = listOfViews.size() - 1;
            listOfViews.remove(index);
            LinearLayout ll = (LinearLayout) findViewById(index);
            layContent.removeView(ll);
        }
    }

    class ItemCategory {
        String catName;

        public ItemCategory(String catName) {
            this.catName = catName;
        }

        public String getCatName() {
            return catName;
        }
    }

    class VPAdapter extends PagerAdapter {

        private final List<ItemCategory> itemList = new ArrayList<>();
        private final List<String> titleItemOfList = new ArrayList<>();
        private final SparseArray<TextView> mHolderArray = new SparseArray<>();
        private LayoutInflater inflater;
        private Context konteks;

        public VPAdapter(Context konteks) {
            this.konteks = konteks;
        }

        @Override
        public int getCount() {
            return itemList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            Ngl.log("destroyItem", "true");
            container.removeView(mHolderArray.get(position));
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            inflater = (LayoutInflater) konteks.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.item_pager_layout, container, false);
            TextView tx = (TextView) view.findViewById(R.id.item_name);
            String itemName = titleItemOfList.get(position);
            tx.setText(itemName);
            return view;
        }

        public void addItem(ItemCategory item, String title) {
            itemList.add(item);
            titleItemOfList.add(title);
        }

        public void resetAdapter() {
            itemList.clear();
            titleItemOfList.clear();
            notifyDataSetChanged();
        }
    }

    @BindView(R.id.input_fragment_name)
    EditText inputFragmentName;
    @BindView(R.id.bt_add_frag)
    Button btAddFrag;
    @BindView(R.id.lay_input)
    LinearLayout layInput;
    @BindView(R.id.lay_path)
    LinearLayout layPath;
    @BindView(R.id.path_container)
    HorizontalScrollView pathContainer;
    @BindView(R.id.vp)
    ViewPager vp;

    @BindView(R.id.activity_dynamic_fragment_viewpager)
    RelativeLayout activityDynamicFragmentViewpager;
    @BindView(R.id.lay_content)
    LinearLayout layContent;
}
