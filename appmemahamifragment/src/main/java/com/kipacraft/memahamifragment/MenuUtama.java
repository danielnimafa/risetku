package com.kipacraft.memahamifragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by danielnimafa on 2/22/17.
 */

public class MenuUtama extends AppCompatActivity {

    private LinkedList<ItemMenu> dataMenu;
    private MenuAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_utama_layout);
        lv = (ListView) findViewById(R.id.list_menu);

        dataMenu = new LinkedList<>();
        adapter = new MenuAdapter(dataMenu, this);

        // add menu
        dataMenu.add(new ItemMenu("Add Fragment Dynamically with ViewPager"));
        adapter.notifyDataSetChanged();

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent in = null;

                switch (position) {
                    case 0:
                        in = new Intent(MenuUtama.this, DynamicFragmentViewpager.class);
                        break;
                }

                startActivity(in);
            }
        });
    }

    // item menu objek
    class ItemMenu {
        String menuName;

        public ItemMenu(String menuName) {
            this.menuName = menuName;
        }

        public String getMenuName() {
            return menuName;
        }
    }

    // adapter list
    class MenuAdapter extends BaseAdapter {

        private LinkedList<ItemMenu> dataMenu;
        private LayoutInflater inflater;
        private Context konteks;

        public MenuAdapter(LinkedList<ItemMenu> dataMenu, Context konteks) {
            this.dataMenu = dataMenu;
            this.inflater = LayoutInflater.from(konteks);
        }

        @Override
        public int getCount() {
            return dataMenu.size();
        }

        @Override
        public ItemMenu getItem(int position) {
            return dataMenu.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = inflater.inflate(R.layout.item_menu_main, null);
            String namaMenu = getItem(position).getMenuName();
            ((TextView) convertView.findViewById(R.id.item_menu))
                    .setText(namaMenu);
            return convertView;
        }
    }

    private ListView lv;
}
