package com.kipacraft.memahamifragment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity implements OnSelectedJudulItemListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        container = (FrameLayout) findViewById(R.id.fragment_container);

        if (container != null) {

            if (savedInstanceState != null) {
                return;
            }

            FragmentJudul fragmentJudul = FragmentJudul.newInstance();
            fragmentJudul.setArguments(getIntent().getExtras());
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, fragmentJudul).commit();
        }
    }

    @Override
    public void onJudulItemSelected(int position) {
        FragmentKonten kontentFrag = (FragmentKonten) getSupportFragmentManager()
                .findFragmentById(R.id.article_fragment);
        if (kontentFrag != null) {

        }
    }

    FrameLayout container;
}
