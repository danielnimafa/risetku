package com.kipacraft.memahamifragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by danielnimafa on 2/23/17.
 */

public class TestFragment extends Fragment {

    public static TestFragment newInstance(String argumen) {

        Bundle args = new Bundle();

        args.putString("text", argumen);

        TestFragment fragment = new TestFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.test_fragment, container, false);
        String arg = getArguments().getString("text");
        ((TextView) v.findViewById(R.id.wow)).setText(arg);
        return v;
    }
}
