package com.kipacraft.memahamifragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by danielnimafa on 2/16/17.
 */

public class FragmentJudul extends Fragment {
    public static final String TAG = FragmentJudul.class.getSimpleName();

    public static FragmentJudul newInstance() {

        Bundle args = new Bundle();

        FragmentJudul fragment = new FragmentJudul();
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentJudul() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Ngl.log(TAG, "onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Ngl.log(TAG, "onCreate");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Ngl.log(TAG, "onCreateView");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        Ngl.log(TAG, "onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        Ngl.log(TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Ngl.log(TAG, "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Ngl.log(TAG, "onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Ngl.log(TAG, "onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Ngl.log(TAG, "onDestroy");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Ngl.log(TAG, "onSaveInstanceState");
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Ngl.log(TAG, "onViewStateRestored");
    }
}
