package com.kipacraft.memahamifragment;

/**
 * Created by danielnimafa on 2/16/17.
 */

public interface OnSelectedJudulItemListener {
    void onJudulItemSelected(int position);
}
