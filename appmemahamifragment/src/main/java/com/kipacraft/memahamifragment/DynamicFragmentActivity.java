package com.kipacraft.memahamifragment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class DynamicFragmentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic_fragment);
    }
}
