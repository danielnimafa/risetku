package com.kipacraft.danielnimafa;

/**
 * Created by Danielnimafa on 19/12/2016.
 */

public class Const {

    // BASE URL
    /*public static String BASE = "http://localhost/febstore/";*/
    /*public static String BASE = "http://febstore.byethost3.com/backend/web/";*/
    public static String BASE = "http://api.febrawijayastore.com/";
    public static String API_BASE_URL = BASE + "v1/";

    public static String namaApp = "";
    public static String IMAGE_DIRECTORY_NAME = "PICTURES";
    public static String IMAGECACHE_DIRECTORY_NAME = "CACHE";

    public static final int PICK_IMAGE_REQUEST = 1;
    public static final int PICK_IMAGE_REQUEST_KITKAT = 2;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int PIC_CROP = 90;

    // name tag
    public static final String token_tag = "token";
    public static final String login_status_tag = "login_status";

    // label set on splash screen
    public static String appName = "";

    public static final String auth = "auth/";
    public static final String authSendEmail = auth + "sendemail";
    public static final String authEmailLogin = auth + "emaillogin";
    public static final String authLogout = auth + "logout";
    public static final String authRegister = auth + "register";
    public static final String authforgotPass = auth + "forgotpassword";
    public static final String about = auth + "about";

    private static final String user = "user/";
    public static final String userEdit = user + "edit";
    public static final String userEditPhoto = user + "editphoto";
    public static final String userGetProfile = user + "getprofile";
    public static final String userChangePass = user + "changepassword";

    private static final String member = "member/";
    public static final String memberCreate = member + "create";
    public static final String memberEdit = member + "edit";
    public static final String memberGet = member + "get";

    private static final String store = "store/";
    public static final String storeClaim = store + "claim";
    public static final String storeEdit = store + "edit";
    public static final String storeCurrent = store + "current";
    public static final String storeReviewList = store + "reviewlist";
    public static final String storeReview = store + "review";
    public static final String storeReviewEdit = store + "reviewedit";

    private static final String product = "product/";
    public static final String productAdd = product + "add";
    public static final String productEdit = product + "edit";
    public static final String productDelete = product + "delete";
    public static final String productReview = product + "review";
    public static final String productGet = product + "get";
    public static final String productCurrent = product + "currentlist";

    public static final String blog = "blog/";
    public static final String blogList = blog + "list";
    public static final String blogGet = blog + "get";

    public static final String productguest = "productguest/";
    public static final String pgList = productguest + "list";
    public static final String pgGetcategory = productguest + "getcategory";
    public static final String pgGet = productguest + "get";
    public static final String pgCategory = productguest + "category";
    public static final String pgSearch = productguest + "search";
    public static final String pgStoreGet = productguest + "getstore";

    public static int DELAYINTERVAl = 100;
}
