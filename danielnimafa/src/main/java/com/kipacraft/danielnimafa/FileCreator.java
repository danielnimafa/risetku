package com.kipacraft.danielnimafa;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Androidev on 6/22/2016.
 * <p>
 * ini class helper untuk hal tentang pembuatan file
 */
public class FileCreator {

    public static String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
    public static String dirPic = Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_PICTURES).toString();
    public static File mainDir = new File(extStorageDirectory
            + File.separator + Const.namaApp + File.separator);

    public static long standardSize = 300;

    public static File makeImgCacheFile() {
        File mediaStorageDir = new File(mainDir, Const.IMAGE_DIRECTORY_NAME
                + File.separator
                + Const.IMAGECACHE_DIRECTORY_NAME);
        System.out.println("Main Dir: " + mediaStorageDir.getPath());

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                System.out.println("ADAPTER: Oops! Failed create "
                        + Const.IMAGE_DIRECTORY_NAME + " directory");
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());

        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "_IMG_CACHE_" + timeStamp + ".jpg");
        return mediaFile;
    }

    public static File getOutputFilePickGallery() {
        System.out.println("DISINI");
        File mediaStorageDir = new File(mainDir, Const.IMAGE_DIRECTORY_NAME);
        System.out.println("Main Dir: " + mediaStorageDir.getPath());

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                System.out.println("ADAPTER: Oops! Failed create "
                        + Const.IMAGE_DIRECTORY_NAME + " directory");
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());

        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_PICKGALLERY_" + timeStamp + ".jpg");
        return mediaFile;
    }

    public static File makeImgCacheFileWithPosition(int pos) {
        File mediaStorageDir = new File(mainDir, Const.IMAGE_DIRECTORY_NAME
                + File.separator
                + Const.IMAGECACHE_DIRECTORY_NAME);
        System.out.println("Main Dir: " + mediaStorageDir.getPath());

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                System.out.println("ADAPTER: Oops! Failed create "
                        + Const.IMAGE_DIRECTORY_NAME + " directory");
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());

        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "_IMG_CACHE_" + timeStamp + "_" + pos + ".jpg");
        return mediaFile;
    }

    public static File getOutputMediaFileCapture() {
        // External sdcard location
        File mediaStorageDir = new File(mainDir, Const.IMAGE_DIRECTORY_NAME);
        System.out.println("Main Dir: " + mediaStorageDir.getPath());

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                System.out.println("ADAPTER: Oops! Failed create "
                        + Const.IMAGE_DIRECTORY_NAME + " directory");
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());

        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_CAPTURE" + timeStamp + ".jpg");
        return mediaFile;
    }

    public static long getFileSize(String path) {
        long length = 0;
        try {
            File pickedImage = new File(path);
            length = pickedImage.length();
            length = length / 1024;
            System.out.println("File Path : " + pickedImage.getPath() + ", File size : "
                    + length + " KB");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return length;
    }

    public static void copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

    public static File savebitmap(String filename) {
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        OutputStream outStream = null;

        File file = new File(filename + ".png");
        if (file.exists()) {
            file.delete();
            file = new File(extStorageDirectory, filename + ".png");
            System.out.println("file exist: " + file + ",Bitmap= " + filename);
        }

        try {
            // make a new bitmap from your file
            Bitmap bitmap = BitmapFactory.decodeFile(file.getName());

            outStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("file: " + file);
        return file;

    }

    public static void createAssetsFolder() {
        File assetFolder = new File(String.valueOf(mainDir));

        if (!assetFolder.exists()) {
            assetFolder.mkdirs();
        }
    }
}
