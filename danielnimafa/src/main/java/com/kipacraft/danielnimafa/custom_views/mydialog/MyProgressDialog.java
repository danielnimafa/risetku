package com.kipacraft.danielnimafa.custom_views.mydialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kipacraft.danielnimafa.R;

/**
 * Created by Danielnimafa on 05/01/2017.
 */

public class MyProgressDialog extends AppCompatDialogFragment {

    public static String TAG = MyProgressDialog.class.getSimpleName();

    public MyProgressDialog() {
    }

    public static MyProgressDialog newInstance() {

        Bundle args = new Bundle();

        MyProgressDialog fragment = new MyProgressDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, R.style.AppDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.dialog_shape);
        return inflater.inflate(R.layout.progress_layout, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
