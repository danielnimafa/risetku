package com.kipacraft.danielnimafa.custom_views;

import android.content.Context;
import android.graphics.Typeface;

import java.lang.reflect.Field;

/**
 * Created by Androidev on 7/25/2016.
 */
public class FontsOverride {

    public static void setDefaulFont(Context konteks, String staticTypefaceFieldName,
                                     String fontAssetName) {
        final Typeface regular = Typeface.createFromAsset(konteks.getAssets(), fontAssetName);
        replaceFont(staticTypefaceFieldName, regular);
    }

    protected static void replaceFont(String staticTypefaceFieldName, Typeface newTypeface) {
        try {
            final Field staticField = Typeface.class.getDeclaredField(staticTypefaceFieldName);
            staticField.setAccessible(true);
            staticField.set(null, newTypeface);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
