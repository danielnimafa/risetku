package com.kipacraft.danielnimafa;

import android.util.Base64;

import java.io.UnsupportedEncodingException;

/**
 * Created by nielmafa on 3/19/2015.
 */
public class KompresiBase64 {

    public KompresiBase64(){}

    public String EncodeBase64String(String konten){
        String hasil = "";
        byte[] data = new byte[0];
        try {
            data = konten.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        hasil = Base64.encodeToString(data, Base64.DEFAULT);
        return hasil;
    }

    public String DecodeBase64String(String base64encoded){
        String hasil = "";
        byte[] data = Base64.decode(base64encoded, Base64.DEFAULT);
        try {
            hasil = new String(data, ("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return hasil;
    }

}
