package com.kipacraft.danielnimafa;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.util.Base64;
import android.util.TypedValue;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Androidev on 6/21/2016.
 */
public class ImageBitmapUtils {

    public static Bitmap resizeImage(String pathPic, double width, double height, int sampleSize) {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = sampleSize;

        Bitmap bitmap = BitmapFactory.decodeFile(pathPic, options);
        Bitmap resized = Bitmap.createScaledBitmap(bitmap,
                (int) (bitmap.getWidth() * width),
                (int) (bitmap.getHeight() * height), true);

        return resized;
    }

    public static Bitmap resizeBitmap(Bitmap bitmap, double width, double height) {
        return Bitmap.createScaledBitmap(bitmap,
                (int) (bitmap.getWidth() * width),
                (int) (bitmap.getHeight() * height), true);
    }

    public static Bitmap getImageBitmap(String pathPic) {
        return BitmapFactory.decodeFile(pathPic);
    }

    public static Bitmap rotateBitmap(Bitmap bitmap, int orientation) {

        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_NORMAL:
                return bitmap;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;
            default:
                return bitmap;
        }

        try {
            Bitmap bmRotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            bitmap.recycle();
            return bmRotated;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Bitmap orientationCheck(String pathPic, Bitmap resized) {
        Bitmap result = null;
        try {
            ExifInterface exif = new ExifInterface(pathPic);
            int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
            int rotationDegrees = exifToDegrees(rotation);

            switch (rotation) {
                case ExifInterface.ORIENTATION_UNDEFINED:
                    result = rotateBitmap(resized, ExifInterface.ORIENTATION_ROTATE_90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    result = rotateBitmap(resized, ExifInterface.ORIENTATION_ROTATE_90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    result = rotateBitmap(resized, ExifInterface.ORIENTATION_ROTATE_180);
                    break;

                default:
                    result = resized;
                    break;
                // etc.
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static Bitmap imageOrientationCheck(String pathPic) {
        Bitmap bitmapResult = null;
        Bitmap imageOri = BitmapFactory.decodeFile(pathPic);
        try {
            ExifInterface exif = new ExifInterface(pathPic);
            int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
            /*int rotationDegrees = exifToDegrees(rotation);*/
            bitmapResult = rotateBitmap(imageOri, rotation);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return bitmapResult;
    }

    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }


    public static String getImagePathNoOrientationCheck(String path) {
        String result = null;
        Bitmap bitmap = null;
        OutputStream outStream = null;

        Bitmap rotatedBitmap = BitmapFactory.decodeFile(path);

        try {
            /* make a new bitmap from your file*/
            File fileGambar = FileCreator.getOutputFilePickGallery();
            outStream = new FileOutputStream(fileGambar);
            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 75, outStream);
            outStream.flush();
            outStream.close();

            result = fileGambar.getPath();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public static String getImagePathNoFiltering(String selectedImagePath) {
        String result = null;
        Bitmap bitmap = null;
        OutputStream outStream = null;

        Bitmap rotatedBitmap = ImageBitmapUtils.imageOrientationCheck(selectedImagePath);

        try {
            /* make a new bitmap from your file*/
            File fileGambar = FileCreator.getOutputFilePickGallery();
            outStream = new FileOutputStream(fileGambar);
            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 75, outStream);
            outStream.flush();
            outStream.close();

            result = fileGambar.getPath();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public static Bitmap decodeSampledBitmapFromFile(String filePath, int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(filePath, options);
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static String buildImageFile(Bitmap correctBitmap, File file) {
        OutputStream outStream = null;
        String result = null;
        try {
            /* make a new bitmap from your file*/
            outStream = new FileOutputStream(file);
            correctBitmap.compress(Bitmap.CompressFormat.JPEG, 75, outStream);
            outStream.flush();
            outStream.close();

            result = file.getPath();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public static String buildImageFileOptimizedSize(Bitmap correctBitmap, File file) {
        OutputStream outStream = null;
        String result = null;
        Bitmap bmp = null;
        try {
            /* make a new bitmap from your file*/
            outStream = new FileOutputStream(file);
            correctBitmap.compress(Bitmap.CompressFormat.JPEG, 75, outStream);
            outStream.flush();
            outStream.close();

            result = file.getPath();
            long size = FileCreator.getFileSize(result);
            long standardSize = FileCreator.standardSize;
            bmp = ImageBitmapUtils.resizeImage(result,
                    (size > standardSize) ? 0.8 : 1,
                    (size > standardSize) ? 0.8 : 1,
                    (size > standardSize) ? 4 : 1);
            try {
                outStream = new FileOutputStream(file);
                bmp.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                outStream.flush();
                outStream.close();

                result = file.getPath();
                long sizeAfter = FileCreator.getFileSize(result);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    // create new image file
    public static String getImagePath(String selectedImagePath) {
        String result = null;
        long size = FileCreator.getFileSize(selectedImagePath);
        long standardSize = FileCreator.standardSize;
        Bitmap bitmap = null;
        OutputStream outStream = null;

        File fileGambar = FileCreator.getOutputFilePickGallery();
        bitmap = ImageBitmapUtils.resizeImage(selectedImagePath,
                (size > standardSize) ? 0.5 : 0.8,
                (size > standardSize) ? 0.5 : 0.8,
                (size > standardSize) ? 4 : 1);
        Bitmap rotatedBitmap = ImageBitmapUtils.orientationCheck(selectedImagePath, bitmap);

        try {
            /* make a new bitmap from your file*/
            outStream = new FileOutputStream(fileGambar);
            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
            outStream.flush();
            outStream.close();

            result = fileGambar.getPath();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    private static int dpToPx(int dp, Context konteks) {
        Resources r = konteks.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    public static String imgToBase64(String pathPic) {
        Bitmap fotoItem = BitmapFactory.decodeFile(pathPic);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        fotoItem.compress(Bitmap.CompressFormat.JPEG, 75, baos);
        byte[] b = baos.toByteArray();
        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encodedImage;
    }

    public static String imgProfil64(String pathpic) {
        Bitmap fotoItem = ImageBitmapUtils.decodeSampledBitmapFromFile(pathpic, 90, 90);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        fotoItem.compress(Bitmap.CompressFormat.JPEG, 75, baos);
        byte[] b = baos.toByteArray();
        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encodedImage;
    }

    public static Bitmap streamingImage(String url) {
        Bitmap result = null;
        try {
            URL myFileUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) myFileUrl.openConnection();
            conn.setDoInput(true);
            conn.connect();
            InputStream is = null;
            try {
                is = conn.getInputStream();
                result = BitmapFactory.decodeStream(is);
            } finally {
                if (is != null) {
                    is.close();
                }
            }
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Bitmap streamingImageOkHTTP(InputStream inputStream) {
        Bitmap result = null;
        try {
            InputStream is = inputStream;
            result = BitmapFactory.decodeStream(is);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Bitmap optimizeBitmap(String filePath, long fileSize) {
        Bitmap result = null;
        long standardSize = FileCreator.standardSize;
        Bitmap resized = imageOrientationCheck(filePath);
        result = (fileSize > standardSize) ? ImageBitmapUtils.resizeBitmap(resized, 0.5, 0.5) : resized;
        return result;
    }
}

