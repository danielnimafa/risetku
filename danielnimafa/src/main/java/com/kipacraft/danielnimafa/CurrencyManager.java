package com.kipacraft.danielnimafa;

import java.text.DecimalFormat;

/**
 * Created by danielnimafa on 06/09/2016.
 */
public class CurrencyManager {
    public static String formatPrice(String price) {
        DecimalFormat formatter = new DecimalFormat("#,###,###,###,###");
        String harga = "Rp. " + formatter.format(Double.parseDouble(price));
        return harga;
    }

    public static String inputFormatPrice(String price) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        String harga = formatter.format(Integer.parseInt(price));
        return harga;
    }
}
