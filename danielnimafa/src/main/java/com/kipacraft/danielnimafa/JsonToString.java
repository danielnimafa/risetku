package com.kipacraft.danielnimafa;

import android.app.Activity;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by danielnimafa on 2/27/17.
 */

public class JsonToString {
    private Activity activity;

    public JsonToString(Activity activity) {
        this.activity = activity;
    }

    public String loadJsonFromAssets(String jsonFileName) {
        String json;

        try {
            InputStream is = activity.getAssets().open(jsonFileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return json;
    }
}
