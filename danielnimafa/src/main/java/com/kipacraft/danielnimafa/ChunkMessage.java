package com.kipacraft.danielnimafa;

import android.util.Log;

import com.google.common.base.Splitter;

import java.util.ArrayList;


/**
 * Created by nielmafa on 3/9/2015.
 */
public class ChunkMessage {

    private JsonKeyFormat jfk = new JsonKeyFormat();
    private String keyTaskId, keyId, keyData;
    private int countKontenData;
    private int limitData = 5;

    public ChunkMessage() {
        keyId = jfk.getKonten("id");
        keyData = jfk.getKonten("dt");
        keyTaskId = jfk.getKonten("ti");
    }

    public ArrayList<String> splitData(String kontenDataSender, String idForm) {
        ArrayList<String> chunkList = new ArrayList<>();
        countKontenData = kontenDataSender.length();
        Log.d("Jumlah Karakter Data", String.valueOf(countKontenData));
        StringBuilder si = new StringBuilder();
        int index = 0;
        for(String dataChunks : Splitter.fixedLength(limitData).split(kontenDataSender)){
            Log.d("Potongan", dataChunks);
            String taskId = String.valueOf(index++);
            String kontenSmsChunks = prepareChunks(idForm, taskId, dataChunks);
            String lengthSmsChunks = String.valueOf(kontenSmsChunks.length());
            Log.d("Hasil Chunks", kontenSmsChunks + "\n" + taskId + "\n" + lengthSmsChunks);
            si.append(kontenSmsChunks + "\n");
            chunkList.add(kontenSmsChunks);
        }
        return chunkList;
//        String hasilKonten = si.toString();
//        return hasilKonten;
    }

    public String prepareChunks(String id, String taskId, String data) {
        String preparedChunks = "{" + keyTaskId + ":" + jfk.getKonten(id) + ","
                + keyId + ":" + jfk.getKonten(taskId) + ","
                + keyData + ":" + jfk.getKonten(data) + "}";
        return preparedChunks;
    }
}
