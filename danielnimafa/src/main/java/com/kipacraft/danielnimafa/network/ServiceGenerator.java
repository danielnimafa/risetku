package com.kipacraft.danielnimafa.network;


import com.kipacraft.danielnimafa.Const;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by danielnimafa on 17/05/2016.
 */
public class ServiceGenerator {

    private String apiBaseUrl;

    public ServiceGenerator(String apiBaseUrl) {
        this.apiBaseUrl = apiBaseUrl;
    }

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
            .readTimeout(480, TimeUnit.SECONDS)
            .connectTimeout(480, TimeUnit.SECONDS);

    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(Const.API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create());


    public static <S> S createService(Class<S> serviceClass) {
        Retrofit retrofit = builder.client(httpClient.build()).build();
        return retrofit.create(serviceClass);
    }

    private static Retrofit.Builder builderString = new Retrofit.Builder().baseUrl(Const.API_BASE_URL)
            .addConverterFactory(new ToStringConverterFactory());

    public static <S> S createServiceString(Class<S> serviceClass) {
        Retrofit retrofit = builderString.client(httpClient.build()).build();
        return retrofit.create(serviceClass);
    }

    private static Retrofit.Builder builderRxJava = new Retrofit.Builder().baseUrl(Const.API_BASE_URL)
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create());

    public static <S> S createServiceRx(Class<S> serviceClass) {
        Retrofit retrofit = builderRxJava.client(httpClient.build()).build();
        return retrofit.create(serviceClass);
    }

    /*Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Const.API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create()).build();
    TaskService callService = retrofit.create(TaskService.class);
    Call<LoginRes> call = callService.AuthEmailLoginObj(Const.ambilJadwalDokter(""));
    call.enqueue(new );*/

}
