package com.kipacraft.danielnimafa.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Androidev on 8/16/2016.
 */
public class InetConnection {

    private boolean isConnected;
    Context konteks;
    private boolean isWifi;

    public InetConnection(Context konteks) {

        this.konteks = konteks;

        /*ConnectivityManager cm =
                (ConnectivityManager) konteks.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        isWifi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;*/
    }

    public boolean isInetConnect() {
        return isConnected;
    }

    public boolean isWifiConnect() {
        return isWifi;
    }

    public boolean internetCheck() {

        ConnectivityManager cm = (ConnectivityManager) konteks.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            isConnected = true;
        } else {
            isConnected = false;
        }

        return isConnected;
    }

}
