package com.kipacraft.appriset.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

/**
 * Created by danieldev on 3/24/17.
 */

public class Alertku {
    static ProgressDialog pd;

    public static void showAlertBox(Activity activity, String pesan) {
        AlertDialog ad = new AlertDialog.Builder(activity).create();
        ad.setMessage(pesan);
        ad.setCanceledOnTouchOutside(false);
        ad.setCancelable(false);
        ad.setButton(DialogInterface.BUTTON_POSITIVE, "dismiss", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        ad.show();
    }

    public static void showProgressDialog(Activity activity, String pesan, boolean state) {
        if (state) {
            pd = new ProgressDialog(activity);
            pd.setMessage(pesan);
            pd.setCanceledOnTouchOutside(false);
            pd.setCancelable(false);
            pd.show();
        } else {
            if (pd != null) {
                if (pd.isShowing()) {
                    pd.dismiss();
                }
            }
        }
    }
}
