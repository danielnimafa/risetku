package com.kipacraft.appriset;

import android.content.Intent;

/**
 * Created by danielnimafa on 3/18/17.
 */

public interface DuaView {
    Intent getintent();

    void tos(String msg);

    void attachData(String appName);
}
