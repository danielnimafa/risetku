package com.kipacraft.appriset;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;

import com.kipacraft.appriset.utils.Alertku;

public class SatuActivity extends AppCompatActivity {

    RelativeLayout rootView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_satu);

        rootView = (RelativeLayout) findViewById(R.id.activity_satu);

        findViewById(R.id.wow).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SatuActivity.this, DuaActivity.class);
                intent.putExtra("wow", "oke");
                startActivity(intent);
            }
        });

        findViewById(R.id.joss).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Alertku.showProgressDialog(SatuActivity.this, "Loading...", true);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Alertku.showProgressDialog(SatuActivity.this, "", false);
                    }
                }, 5000);
            }
        });

        findViewById(R.id.show).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Alertku.showAlertBox(SatuActivity.this, "JOSS");
            }
        });
    }


}
