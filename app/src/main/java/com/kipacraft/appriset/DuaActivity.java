package com.kipacraft.appriset;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.kipacraft.danielnimafa.Ngl;

public class DuaActivity extends AppCompatActivity implements DuaView {

    Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dua);
        if (presenter == null) {
            presenter = new Presenter(this, this, this);
        }
        presenter.onCreate(savedInstanceState);
    }

    @Override
    public Intent getintent() {
        return getIntent();
    }

    @Override
    public void tos(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void attachData(String appName) {
        ((TextView) findViewById(R.id.hasil)).setText(appName);
    }

    // --------------------------------------------------------------------------------------------

    class Presenter {
        DuaView mv;
        Activity activity;
        Context konteks;

        public Presenter(DuaView mv, Activity activity, Context konteks) {
            this.mv = mv;
            this.activity = activity;
            this.konteks = konteks;
        }

        public void onCreate(Bundle savedInstanceState) {
            initObj();
        }

        private void initObj() {
            Intent in = getIntent();
            if (in != null) {
                Ngl.log("intent", "not null");
                String wow = in.getStringExtra("wow");
                mv.tos(wow);
                String appName = getResources().getString(R.string.app_name);
                mv.attachData(appName);
            } else {
                Ngl.log("intent", "null");
            }
        }
    }
}
